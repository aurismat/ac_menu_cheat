# AssaultCube simple trainer/cheat

My own attempt at creating AssaultCube cheat/trainer/whatever you want to call it.

I'm using

**What I have made so far**
*  Infinite HP;
*  Infinite AP;
*  Speedhack(**very** linear and works only in forward, use with caution);
*  Triggerbot;
*  Pistol akimbo switch;
*  No recoil;
*  Rapid fire;
*  Infinite ammo toggle(including nades!).

**How to use**
*  Clone the repo;
*  Source the VAMemory.dll;
*  Compile with Visual Studio 2017(or up, not sure);
*  Start up AssaultCube(latest version of 1.2.0.2 should work);
*  Start the trainer;
*  Profit!

**Credits**
*  VAMemory(also a dependency, source it yourself);