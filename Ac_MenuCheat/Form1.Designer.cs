﻿namespace Ac_MenuCheat
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.hpToggle = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.TriggerbotToggle = new System.Windows.Forms.CheckBox();
            this.RapidFireToggle = new System.Windows.Forms.CheckBox();
            this.RCStoggle = new System.Windows.Forms.CheckBox();
            this.AkimboToggle = new System.Windows.Forms.CheckBox();
            this.SpeedLabel = new System.Windows.Forms.Label();
            this.SpeedUp = new System.Windows.Forms.Button();
            this.SpeedDown = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.AkimboAmmoToggle = new System.Windows.Forms.CheckBox();
            this.SMGAmmoToggle = new System.Windows.Forms.CheckBox();
            this.CarbineAmmoToggle = new System.Windows.Forms.CheckBox();
            this.SniperAmmoToggle = new System.Windows.Forms.CheckBox();
            this.ShotgunAmmoToggle = new System.Windows.Forms.CheckBox();
            this.NadeAmmoToggle = new System.Windows.Forms.CheckBox();
            this.ArmorToggle = new System.Windows.Forms.CheckBox();
            this.PistolAmmoToggle = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ARAmmoToggle = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // hpToggle
            // 
            this.hpToggle.AutoSize = true;
            this.hpToggle.Location = new System.Drawing.Point(14, 34);
            this.hpToggle.Name = "hpToggle";
            this.hpToggle.Size = new System.Drawing.Size(41, 17);
            this.hpToggle.TabIndex = 0;
            this.hpToggle.Text = "HP";
            this.hpToggle.UseVisualStyleBackColor = true;
            this.hpToggle.CheckedChanged += new System.EventHandler(this.hpToggle_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.TriggerbotToggle);
            this.panel1.Controls.Add(this.RapidFireToggle);
            this.panel1.Controls.Add(this.RCStoggle);
            this.panel1.Controls.Add(this.AkimboToggle);
            this.panel1.Controls.Add(this.SpeedLabel);
            this.panel1.Controls.Add(this.SpeedUp);
            this.panel1.Controls.Add(this.SpeedDown);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.AkimboAmmoToggle);
            this.panel1.Controls.Add(this.SMGAmmoToggle);
            this.panel1.Controls.Add(this.CarbineAmmoToggle);
            this.panel1.Controls.Add(this.SniperAmmoToggle);
            this.panel1.Controls.Add(this.ShotgunAmmoToggle);
            this.panel1.Controls.Add(this.NadeAmmoToggle);
            this.panel1.Controls.Add(this.ArmorToggle);
            this.panel1.Controls.Add(this.PistolAmmoToggle);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.ARAmmoToggle);
            this.panel1.Controls.Add(this.hpToggle);
            this.panel1.Controls.Add(this.label2);
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(228, 205);
            this.panel1.TabIndex = 1;
            // 
            // TriggerbotToggle
            // 
            this.TriggerbotToggle.AutoSize = true;
            this.TriggerbotToggle.Location = new System.Drawing.Point(14, 117);
            this.TriggerbotToggle.Name = "TriggerbotToggle";
            this.TriggerbotToggle.Size = new System.Drawing.Size(78, 17);
            this.TriggerbotToggle.TabIndex = 18;
            this.TriggerbotToggle.Text = "Trigger Bot";
            this.TriggerbotToggle.UseVisualStyleBackColor = true;
            this.TriggerbotToggle.CheckedChanged += new System.EventHandler(this.TriggerbotToggle_CheckedChanged);
            // 
            // RapidFireToggle
            // 
            this.RapidFireToggle.AutoSize = true;
            this.RapidFireToggle.Location = new System.Drawing.Point(14, 186);
            this.RapidFireToggle.Name = "RapidFireToggle";
            this.RapidFireToggle.Size = new System.Drawing.Size(74, 17);
            this.RapidFireToggle.TabIndex = 17;
            this.RapidFireToggle.Text = "Rapid Fire";
            this.RapidFireToggle.UseVisualStyleBackColor = true;
            this.RapidFireToggle.CheckedChanged += new System.EventHandler(this.checkBox8_CheckedChanged);
            // 
            // RCStoggle
            // 
            this.RCStoggle.AutoSize = true;
            this.RCStoggle.Location = new System.Drawing.Point(14, 163);
            this.RCStoggle.Name = "RCStoggle";
            this.RCStoggle.Size = new System.Drawing.Size(73, 17);
            this.RCStoggle.TabIndex = 16;
            this.RCStoggle.Text = "No Recoil";
            this.RCStoggle.UseVisualStyleBackColor = true;
            this.RCStoggle.CheckedChanged += new System.EventHandler(this.checkBox7_CheckedChanged);
            // 
            // AkimboToggle
            // 
            this.AkimboToggle.AutoSize = true;
            this.AkimboToggle.Location = new System.Drawing.Point(14, 140);
            this.AkimboToggle.Name = "AkimboToggle";
            this.AkimboToggle.Size = new System.Drawing.Size(97, 17);
            this.AkimboToggle.TabIndex = 15;
            this.AkimboToggle.Text = "Akimbo Toggle";
            this.AkimboToggle.UseVisualStyleBackColor = true;
            this.AkimboToggle.CheckedChanged += new System.EventHandler(this.checkBox6_CheckedChanged);
            // 
            // SpeedLabel
            // 
            this.SpeedLabel.AutoSize = true;
            this.SpeedLabel.Location = new System.Drawing.Point(44, 100);
            this.SpeedLabel.Name = "SpeedLabel";
            this.SpeedLabel.Size = new System.Drawing.Size(38, 13);
            this.SpeedLabel.TabIndex = 14;
            this.SpeedLabel.Text = "Speed";
            // 
            // SpeedUp
            // 
            this.SpeedUp.Location = new System.Drawing.Point(65, 95);
            this.SpeedUp.Name = "SpeedUp";
            this.SpeedUp.Size = new System.Drawing.Size(23, 23);
            this.SpeedUp.TabIndex = 13;
            this.SpeedUp.Text = "+";
            this.SpeedUp.UseVisualStyleBackColor = true;
            this.SpeedUp.Click += new System.EventHandler(this.SpeedUp_Click);
            // 
            // SpeedDown
            // 
            this.SpeedDown.Location = new System.Drawing.Point(14, 94);
            this.SpeedDown.Name = "SpeedDown";
            this.SpeedDown.Size = new System.Drawing.Size(23, 23);
            this.SpeedDown.TabIndex = 12;
            this.SpeedDown.Text = "-";
            this.SpeedDown.UseVisualStyleBackColor = true;
            this.SpeedDown.Click += new System.EventHandler(this.SpeedDown_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label3.Location = new System.Drawing.Point(11, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Speed";
            // 
            // AkimboAmmoToggle
            // 
            this.AkimboAmmoToggle.AutoSize = true;
            this.AkimboAmmoToggle.Location = new System.Drawing.Point(127, 186);
            this.AkimboAmmoToggle.Name = "AkimboAmmoToggle";
            this.AkimboAmmoToggle.Size = new System.Drawing.Size(94, 17);
            this.AkimboAmmoToggle.TabIndex = 10;
            this.AkimboAmmoToggle.Text = "Akimbo Pistols";
            this.AkimboAmmoToggle.UseVisualStyleBackColor = true;
            this.AkimboAmmoToggle.CheckedChanged += new System.EventHandler(this.AkimboAmmoToggle_CheckedChanged);
            // 
            // SMGAmmoToggle
            // 
            this.SMGAmmoToggle.AutoSize = true;
            this.SMGAmmoToggle.Location = new System.Drawing.Point(127, 163);
            this.SMGAmmoToggle.Name = "SMGAmmoToggle";
            this.SMGAmmoToggle.Size = new System.Drawing.Size(50, 17);
            this.SMGAmmoToggle.TabIndex = 9;
            this.SMGAmmoToggle.Text = "SMG";
            this.SMGAmmoToggle.UseVisualStyleBackColor = true;
            this.SMGAmmoToggle.CheckedChanged += new System.EventHandler(this.SMGAmmoToggle_CheckedChanged);
            // 
            // CarbineAmmoToggle
            // 
            this.CarbineAmmoToggle.AutoSize = true;
            this.CarbineAmmoToggle.Location = new System.Drawing.Point(127, 140);
            this.CarbineAmmoToggle.Name = "CarbineAmmoToggle";
            this.CarbineAmmoToggle.Size = new System.Drawing.Size(86, 17);
            this.CarbineAmmoToggle.TabIndex = 8;
            this.CarbineAmmoToggle.Text = "Carbine Rifle";
            this.CarbineAmmoToggle.UseVisualStyleBackColor = true;
            this.CarbineAmmoToggle.CheckedChanged += new System.EventHandler(this.CarbineAmmoToggle_CheckedChanged);
            // 
            // SniperAmmoToggle
            // 
            this.SniperAmmoToggle.AutoSize = true;
            this.SniperAmmoToggle.Location = new System.Drawing.Point(127, 117);
            this.SniperAmmoToggle.Name = "SniperAmmoToggle";
            this.SniperAmmoToggle.Size = new System.Drawing.Size(80, 17);
            this.SniperAmmoToggle.TabIndex = 7;
            this.SniperAmmoToggle.Text = "Sniper Rifle";
            this.SniperAmmoToggle.UseVisualStyleBackColor = true;
            this.SniperAmmoToggle.CheckedChanged += new System.EventHandler(this.SniperAmmoToggle_CheckedChanged);
            // 
            // ShotgunAmmoToggle
            // 
            this.ShotgunAmmoToggle.AutoSize = true;
            this.ShotgunAmmoToggle.Location = new System.Drawing.Point(127, 94);
            this.ShotgunAmmoToggle.Name = "ShotgunAmmoToggle";
            this.ShotgunAmmoToggle.Size = new System.Drawing.Size(66, 17);
            this.ShotgunAmmoToggle.TabIndex = 6;
            this.ShotgunAmmoToggle.Text = "Shotgun";
            this.ShotgunAmmoToggle.UseVisualStyleBackColor = true;
            this.ShotgunAmmoToggle.CheckedChanged += new System.EventHandler(this.ShotgunAmmoToggle_CheckedChanged);
            // 
            // NadeAmmoToggle
            // 
            this.NadeAmmoToggle.AutoSize = true;
            this.NadeAmmoToggle.Location = new System.Drawing.Point(127, 71);
            this.NadeAmmoToggle.Name = "NadeAmmoToggle";
            this.NadeAmmoToggle.Size = new System.Drawing.Size(72, 17);
            this.NadeAmmoToggle.TabIndex = 5;
            this.NadeAmmoToggle.Text = "Grenades";
            this.NadeAmmoToggle.UseVisualStyleBackColor = true;
            this.NadeAmmoToggle.CheckedChanged += new System.EventHandler(this.nadeToggle_CheckedChanged);
            // 
            // ArmorToggle
            // 
            this.ArmorToggle.AutoSize = true;
            this.ArmorToggle.Location = new System.Drawing.Point(14, 58);
            this.ArmorToggle.Name = "ArmorToggle";
            this.ArmorToggle.Size = new System.Drawing.Size(53, 17);
            this.ArmorToggle.TabIndex = 2;
            this.ArmorToggle.Text = "Armor";
            this.ArmorToggle.UseVisualStyleBackColor = true;
            this.ArmorToggle.CheckedChanged += new System.EventHandler(this.ArmorToggle_CheckedChanged);
            // 
            // PistolAmmoToggle
            // 
            this.PistolAmmoToggle.AutoSize = true;
            this.PistolAmmoToggle.Location = new System.Drawing.Point(127, 48);
            this.PistolAmmoToggle.Name = "PistolAmmoToggle";
            this.PistolAmmoToggle.Size = new System.Drawing.Size(51, 17);
            this.PistolAmmoToggle.TabIndex = 4;
            this.PistolAmmoToggle.Text = "Pistol";
            this.PistolAmmoToggle.UseVisualStyleBackColor = true;
            this.PistolAmmoToggle.CheckedChanged += new System.EventHandler(this.secondaryWeaponToggle_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label1.Location = new System.Drawing.Point(11, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Infinite HP/AP";
            // 
            // ARAmmoToggle
            // 
            this.ARAmmoToggle.AutoSize = true;
            this.ARAmmoToggle.Location = new System.Drawing.Point(127, 25);
            this.ARAmmoToggle.Name = "ARAmmoToggle";
            this.ARAmmoToggle.Size = new System.Drawing.Size(84, 17);
            this.ARAmmoToggle.TabIndex = 2;
            this.ARAmmoToggle.Text = "Assault Rifle";
            this.ARAmmoToggle.UseVisualStyleBackColor = true;
            this.ARAmmoToggle.CheckedChanged += new System.EventHandler(this.primaryAmmoToggle_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label2.Location = new System.Drawing.Point(124, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Infinite Ammo";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(252, 229);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(268, 268);
            this.MinimumSize = new System.Drawing.Size(268, 268);
            this.Name = "Form1";
            this.Text = "CakeWare";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox hpToggle;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox ArmorToggle;
        private System.Windows.Forms.CheckBox ARAmmoToggle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox NadeAmmoToggle;
        private System.Windows.Forms.CheckBox PistolAmmoToggle;
        private System.Windows.Forms.CheckBox RapidFireToggle;
        private System.Windows.Forms.CheckBox RCStoggle;
        private System.Windows.Forms.CheckBox AkimboToggle;
        private System.Windows.Forms.Label SpeedLabel;
        private System.Windows.Forms.Button SpeedUp;
        private System.Windows.Forms.Button SpeedDown;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox AkimboAmmoToggle;
        private System.Windows.Forms.CheckBox SMGAmmoToggle;
        private System.Windows.Forms.CheckBox CarbineAmmoToggle;
        private System.Windows.Forms.CheckBox SniperAmmoToggle;
        private System.Windows.Forms.CheckBox ShotgunAmmoToggle;
        private System.Windows.Forms.CheckBox TriggerbotToggle;
    }
}

