﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Ac_MenuCheat
{
    public partial class Form1 : Form
    {
        #region Memory addresses and offsets
        //Base addresses
        public static int Base = 0x400000;
        public static int OffsetLocalPlayer = 0x10F4F4;
        public static int LocalPlayer = Base + OffsetLocalPlayer;

        //LocalPlayer coordinate offsets
        public static int oXcoord = 0x4;
        public static int oYcoord = 0x8;
        public static int oZcoord = 0xC;

        //LocalPlayer Data offsets
        public static int oHealth = 0xF8;
        public static int oArmor = 0xFC;
        public static int oAkimboToggle = 0x10C;
        public static int oPlayerSpeed = 0x80;
        public static int oFire = 0x224;
        public static int oJump = 0x18;
        public static int aTargetName = 0x501C38;
        public static int aMouseShotSensitivity = 0x4EE444;

        //LocalPlayer offsets for weapon magazine ammo
        public static int oARAmmo = 0x0150;
        public static int oPistolAmmo = 0x013C;
        public static int oAkimboAmmo = 0x15C;
        public static int oSMGAmmo = 0x148;
        public static int oSniperAmmo = 0x14C;
        public static int oShotgunAmmo = 0x144;
        public static int oRifleAmmo = 0x140;
        public static int oNadeAmmo = 0x158;

        public static int oShotgunTimer = 0x16C;
        public static int oSniperTimer = 0x174;
        public static int oRifleTimer = 0x168;
        #endregion
        #region Form creation and management functions
        public Form1()
        {
            InitializeComponent();
        }
        System.Threading.Thread cheat;
        private void Form1_Load(object sender, EventArgs e)
        {
            this.FormClosing += new FormClosingEventHandler(Form1_FormClosing);
            cheat = new System.Threading.Thread(RunCheat);
            cheat.Start();
        }
        private void Form1_FormClosing(Object sender, FormClosingEventArgs e)
        {
            cheat.Abort();
        }
        #endregion
        #region Controls
        private void hpToggle_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void ArmorToggle_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void primaryAmmoToggle_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void secondaryWeaponToggle_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void nadeToggle_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox8_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {

        }

        //Speed modification controls
        public int speedModifier = 1;
        private void SpeedDown_Click(object sender, EventArgs e)
        {
            if (speedModifier > 1)
                speedModifier--;
        }

        private void SpeedUp_Click(object sender, EventArgs e)
        {
            if (speedModifier < 10)
                speedModifier++;
        }

        private void TriggerbotToggle_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void ShotgunAmmoToggle_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void SniperAmmoToggle_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void CarbineAmmoToggle_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void SMGAmmoToggle_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void AkimboAmmoToggle_CheckedChanged(object sender, EventArgs e)
        {

        }
        #endregion
        public void RunCheat()
        {
            VAMemory vam = new VAMemory("ac_client");
            int local = vam.ReadInt32((IntPtr)Base + OffsetLocalPlayer);
            #region Final addresses
            //Adresses with offsets to use with VAM
            int aHealth = local + oHealth;
            int aArmor = local + oArmor;
            int aAkimbo = local + oAkimboToggle;
            int aSpeed = local + oPlayerSpeed;
            int aShoot = local + oFire;
            int aJump = local + oJump;

            //LocalPlayer Weapon ammo adresses
            int aARAmmo = local + oARAmmo;
            int aPistolAmmo = local + oPistolAmmo;
            int aAkimboAmmo = local + oAkimboAmmo;
            int aSmgAmmo = local + oSMGAmmo;
            int aSniperAmmo = local + oSniperAmmo;
            int aShotgunAmmo = local + oShotgunAmmo;
            int aRifleAmmo = local + oRifleAmmo;
            int aNadeAmmo = local + oNadeAmmo;

            int aShotgunTimer = local + oShotgunTimer;
            int aSniperTimer = local + oSniperTimer;
            int aRifleTimer = local + oRifleTimer;

            //LocalPlayer position coordinates
            int aXvalue = local + oXcoord;
            int aYvalue = local + oYcoord;
            int aZvalue = local + oZcoord;

            #endregion
            while (true)
            {
                SpeedLabel.Text = String.Format("{0}", speedModifier);
                //HP-Armor Toggles
                if(hpToggle.Checked) //HP toggle
                    vam.WriteInt32((IntPtr)aHealth, 100);
                if (ArmorToggle.Checked) //Armor toggle
                    vam.WriteInt32((IntPtr)aArmor, 100);
                //Other LocalPlayer toggles
                if (AkimboToggle.Checked) //Akimbo Powerup Toggle
                    vam.WriteInt32((IntPtr)aAkimbo, 1);
                if (RCStoggle.Checked) //No Recoil
                    vam.WriteInt32((IntPtr)aMouseShotSensitivity, 0);
                if(RapidFireToggle.Checked) //Rapid-Fire
                {
                    vam.WriteInt32((IntPtr)aShotgunTimer, 0);
                    vam.WriteInt32((IntPtr)aRifleTimer, 0);
                    vam.WriteInt32((IntPtr)aSniperTimer, 0);
                }
                //Speed modifier
                if (vam.ReadInt32((IntPtr)aSpeed) <= 1)
                    vam.WriteInt32((IntPtr)aSpeed,
                        (vam.ReadInt32((IntPtr)aSpeed) * speedModifier));

                //Ammo toggle
                if (ARAmmoToggle.Checked)
                    vam.WriteInt32((IntPtr)aARAmmo, 20); //AR
                if (PistolAmmoToggle.Checked)
                    vam.WriteInt32((IntPtr)aPistolAmmo, 10); //Pistol
                if (NadeAmmoToggle.Checked)
                    vam.WriteInt32((IntPtr)aNadeAmmo, 3); //Grenade
                if (ShotgunAmmoToggle.Checked)
                    vam.WriteInt32((IntPtr)aShotgunAmmo, 7); //Shotgun
                if (SniperAmmoToggle.Checked)
                    vam.WriteInt32((IntPtr)aSniperAmmo, 5); //Sniper Rifle
                if (CarbineAmmoToggle.Checked)
                    vam.WriteInt32((IntPtr)aRifleAmmo, 10); //Carbine Rifle
                if (SMGAmmoToggle.Checked)
                    vam.WriteInt32((IntPtr)aSmgAmmo, 30); //SMG
                if (AkimboAmmoToggle.Checked)
                    vam.WriteInt32((IntPtr)aAkimboAmmo, 20); //Akimbo Pistols

                //Triggerbot
                if(TriggerbotToggle.Checked)
                    if (vam.ReadInt32((IntPtr)aTargetName) > 0)
                    {
                        vam.WriteInt32((IntPtr)aShoot, 1);
                        vam.WriteInt32((IntPtr)aTargetName, 0);
                    }

                Thread.Sleep(10);
            }
        }
    }
}
